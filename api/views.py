from django.shortcuts import render
from itsdangerous import Serializer
from .models import Student
from .serializers import StudentSerializer
from rest_framework.renderers import JSONRenderer
from django.http import HttpResponse

# Create your views here.
def student_detail(request, pk):
    stu = Student.objects.get(id=pk)
    print(stu)
    serializer = StudentSerializer(stu)
    print(serializer)
    print(serializer.data)
    json_data = JSONRenderer().render(serializer.data)
    print(json_data)
    return HttpResponse(json_data, content_type = 'application/json')


# Complex DataType --------------> Python Native DataType -----------------> Json Data
                #  Serialization                          Render into Json