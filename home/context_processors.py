

from home.models import Category


def header_context(request):
    context = {
        'all_categories': Category.objects.all()
    }
    return context