from re import U
# from django.core import validators
from django import forms
from .models import Post, Category, User, Tags
from django.contrib.auth.forms import UserCreationForm, UserChangeForm


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['author', 'category', 'title', 'content', 'photo', 'autphoto', 'tags']

   
        # choices = Category.objects.all().values_list( 'name', 'name' )

        # choice_list = []

        # for item in choices:
        #     choice_list.append(item)

        # widgets= {
        #     'category': forms.Select(choices=choice_list, attrs={'class': 'form-control'}),
        # }

        def __init__(self, *args, **kwargs):
            super(forms.ModelForm, self).__init__(*args, **kwargs)
            self.fields['category'].choices = [(l.id, l.category) for l in Category.objects.all()]
            self.fields['tags'].choices = [(l.id, l.tags) for l in Tags.objects.all()]

            choices1 = Tags.objects.all().values_list( 'name', 'name' )

            choice_list1 = []

            for i in choices1:
                choice_list1.append(i)

            self.fields['tags'].choices = choice_list1
            self.widgets['tags'] = forms.Select(choices=choice_list1, attrs={'class': 'form-control'})



            choices = Category.objects.all().values_list( 'name', 'name' )

            choice_list = []

            for item in choices:
                choice_list.append(item)

            self.fields['category'].choices = choice_list

            self.widgets['category'] = forms.Select(choices=choice_list, attrs={'class': 'form-control'})
            # self.tags['tags'] = forms.Select(choices=choice_list1, attrs={'class': 'form-control'})

            # widgets= {
            #     'category': forms.Select(choices=choice_list, attrs={'class': 'form-control'}),
            # }




class Userupdate(forms.Form):
    username = forms.CharField()
    first_name = forms.CharField()
    last_name = forms.CharField()
    email = forms.EmailField()

    def save(self, user):
        user.first_name = self.first_name
        user.last_name = self.last_name
        user.username = self.username
        user.email = self.email       
        user.save()

    # def save(self, user):
    #     data = self.cleaned_data
    #     user = User(username=data['username'], email=data['email'], first_name=data['first_name'],
    #         last_name=data['last_name'])
    #     user.save()

    # def save(self, user):
    #     user_obj = User.objects.get(user=self.username)
    #     user_obj.first_name = self.first_name
    #     user_obj.last_name = self.last_name        
    #     user_obj.email = self.email       
    #     user_obj.save()

class SignUpForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email','address','mobile_number', 'userphoto', 'description']



class EditUserProfileForm(UserChangeForm):
    password = None
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name','email','address','mobile_number',  'userphoto', 'description']
        