from django.db import models
from datetime import datetime
from django.urls import reverse
from django.utils.text import slugify
import string
import random
from django.template.defaultfilters import slugify
from django.contrib.auth.models import AbstractUser
from django.contrib.auth import get_user_model
from django.conf import settings


class Category(models.Model):
    name = models.CharField(max_length=50, unique=True)
    slug = models.SlugField(max_length=100, unique=True, null=True, blank=True)

    class Meta: 
        verbose_name = "Category"
        verbose_name_plural = "Categories"
 
    def __str__(self):  
        return self.name

    def slug(self):  
        N = 7
        res = ''.join(random.choices(string.ascii_uppercase +
                             string.digits, k = N))
        return slugify(self.name) + "-" + str(res)

class User(AbstractUser):
    address = models.CharField(max_length=100, blank=True)
    mobile_number = models.CharField(max_length=10, unique=True)
    userphoto = models.ImageField(upload_to="myimage/")
    description = models.CharField(max_length=50)


class Tags(models.Model):
    tag = models.CharField(max_length=50, unique=True)

    class Meta: 
        verbose_name = "Tag"
        verbose_name_plural = "Tags"

    def __str__(self):
        return self.tag


user_model=get_user_model()
class Post(models.Model):

    author = models.ForeignKey(user_model, on_delete=models.CASCADE, related_name="posts")
    title = models.CharField(max_length=50)
    content = models.TextField()
    date = models.DateField(default=datetime.today)
    photo = models.ImageField(upload_to="myimage/")
    autphoto = models.ImageField(upload_to="myimage/",null=True, blank=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, default= True, null=False)
    slug = models.SlugField(max_length=100, unique=True,null=True, blank=True)
    tags =  models.ManyToManyField(Tags)


    def slug(self):  
        N = 7
        res = ''.join(random.choices(string.ascii_uppercase +
                             string.digits, k = N))
        return slugify(self.title) + "-" + str(res)

    def __str__(self):  
        return self.title 

    # def save(self, *args, **kwargs):
    #     # obj = Post.objects.get(id=1)
    #     # set something
    #     self.slug= slugify(self.title)
    #     super().save(*args, **kwargs)
    #     # obj.save()
    #     # do another something
   
