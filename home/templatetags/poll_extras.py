from django import template
from..models import Category

register = template.Library()

@register.simple_tag
def cat():
    category = Category.objects.all()
    return category

# @register.simple_tag
# def cat():
#     a = 200
#     b = 33

#     if a > b:
#         return True