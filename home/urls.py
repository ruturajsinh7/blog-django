from django.urls import path
from home import views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth.decorators import login_required

urlpatterns = [
    path('', views.index, name="home"),
    path('login', views.loginUser, name="login"),
    path('logout', views.logoutUser, name="logout"),
    path('register', views.register, name="register"),
    path('index2/', views.index2 , name="index2"),
    path('cat/', views.cat , name="cat"),
    path('about/', views.about , name="about"),
    path('elements/', views.elements , name="elements"),
    path('archive/', views.archive , name="archive"),
    path('contact/', views.contact , name="contact"),
    path('blog/', views.blog , name="blog"),
    path('blog_single/', views.blog_single , name="blog_single"),
    path('page_404/', views.page_404 , name="page_404"),
    path('privacy/', views.privacy , name="privacy"),
    path('author/', views.author , name="author"),
    path('author_single/', views.author_single , name="author_single"),
    path('tags/', views.tags , name="tags"),
    path('AddTags/', login_required(views.AddTagView.as_view()) , name="add_tags"),
    # path('tag_sigle/', views.tag_single , name="tag_single"),
    path('categorie_single/', views.categorie_single , name="categorie_single"),
    path('contact2/', views.contact2 , name="contact2"),
    path('AddCategory/', login_required(views.AddCategoryView.as_view()) , name="add_category"),
    path('category_single/', views.category_single , name="category_single"),
    path('index3/<int:pk>', views.Index3DetailView.as_view() , name="index3"),
    path('index3/', views.Index3ListView.as_view(), name='index3list'),
    path('index3/', views.index3 , name="index3"),
    path('categories/<int:pk>', views.CategoriesDetailView.as_view() , name="categories"),
    path('categories/', views.CategoriesListView.as_view() , name="categorieslist"),
    path('change_pass/', views.change_pass, name="change_pass"),
    path('update/', views.UpdateTemplateView.as_view(), name="update"),
    path('profile/', views.user_profile , name="profile"),
    path('author/<int:pk>', views.AuthorDetailView.as_view() , name="authordetailview"),
  
    path('tags/<int:pk>', views.TagsDetailView.as_view() , name="tagsdetailview"),
    # path('tags/<slug:tag_slug>/', views.TagIndexView.as_view(), name='posts_by_tag'),


] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)